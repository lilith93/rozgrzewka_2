const expect = require('chai').expect;
const server = require('../app');

describe('Server GET /', function () {
    let connection;

    // Arrange
    beforeEach(async () => {
        connection = await server.init();
    });

    afterEach(async () => {
        await connection.stop();
    });

    it('should response with status code 200', async () => {
        // Act
        const res = await connection.inject({
            method: 'GET',
            url: '/'
        });

        // Assert
        expect(res.statusCode).to.equal(200);
    });

    it('should return proper JSON response', async () => {
        // Arrange
        var expectedPayload = {
            greeting: 'Hello!',
            language: 'en'
        };
        
        // Act
        const res = await connection.inject({
            method: 'GET',
            url: '/'
        });

        // Assert
        const payload = JSON.parse(res.payload)
        expect(payload).to.be.eql(expectedPayload);
    });
});